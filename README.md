# Encore20

Challenges categories
=====================
```
├── pwn
│   ├── booof
│   ├── boye
│   └── sea_shell
│
├── crypto
│   ├── rsa_madness
│   └── turing
│
├── forensics
│   ├── zippocrack
│   └── bird_chirping
│
├── web
│   ├── ext
│   ├── Circus
│   ├── my_name_is_giff
│   └── weather
│
├── linux
│   └── ext
│
├── reversing
│
├── miscellaneous
│   ├── justin's_reddit_pass
│   ├── sanity_check
│   ├── insanity_check
│   └── rain
│
├── osint
│   ├── justin's_reddit_pass
│   └── rain
│
└── Cryptic
```
