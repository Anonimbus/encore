# Birds_chirping
who knew chirp could lead to a hidden message

## Flag
```
enc0re{sp3c7R0_5st1_tr4ns}
```

## Making of chall
gimp to make a 320x240 .ppm file and use `./encode image.ppm output.wav` \
https://github.com/xdsopl/robot36

## Solution
1. `./decode output.wav image.ppm`
