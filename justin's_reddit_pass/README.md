# <MISC> Justin's reddit password
Justin has been acting weird on internet. It is your job to find out his password and report it. \
flag format: enc0re{password}

## Flag
```
enc0re{case}
```

## Solution
https://reddit.com/etc/passwd/ \
copy justin's hash and crack it
