# My Name is Giff
giff jeff who cares. find the hidden flag

## Flag
```
enc0re{m41_n4m3_iss_y3ffff}
```

## Solution
1. Find the password and username gifs
2. Strip the gifs into frames and count the number of flags and space means space
3. When decoded using octal numbers, you will get the username and password
4. Login to the website using creds and you will get the flag in the title