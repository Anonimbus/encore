# Turning
<pre>
M4 
UKW
Gamma 2 4 
5 9 
14 3 
5 20 
en cr ys it al xh wd
</pre>

cipher text: pogaekucczigdsgwkvb
```
Flag format: encore{roman numbers separated by _ and random string after decryption}
```

## Flag
```
encore{I_VI_VIII_ENCOREHMIKUEHMULMAO}
encore{I_VI_VIII_HMIKUEHMULMAO}
```