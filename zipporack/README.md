# zippocrack
My friend hid the flag deep in the zip files. To make things worse, he put password on them ;-;

## Flag
```
enc0re{r3CuR51vE_ZIP_cR4ck}
```

## Solution
1. use fcrackzip on every zip file with rockyou as the wordlist
